/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing;

import com.ernest.ticketing.entity.EntityModel;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Darlington Otchere
 */
@Stateful
public class TicketingService {

    @PersistenceContext(name = "com.ernest_ticketing_war_1.0-SNAPSHOTPU")
    EntityManager em;

    public EntityModel saveMethod(EntityModel entityModel) {
        em.merge(entityModel);
        return entityModel;
    }

    public EntityModel findMethod(String id) {
        return em.find(EntityModel.class, id);
    }

    public boolean deleteMethod(EntityModel model) {
        try {
            model.setDeleted(true);
            em.merge(model);
            return true;

        } catch (Exception e) {
            return false;
        }

    }
}
