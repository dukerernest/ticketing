/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Ernest
 */

@Entity(name = "Bus")
@Getter
@Setter
public class Bus extends EntityModel{
   
    @Column(name = "bus_number")
    private String busNumber;
    
    @Column(name = "seating_capacity")
    private int seatingCapacity;
    
}
