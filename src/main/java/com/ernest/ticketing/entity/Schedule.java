/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */
@Entity(name = "Schedule")
@Getter
@Setter

public class Schedule extends EntityModel{
    
    @Column(name = "schedule_date")
    private Date scheduleDate;
    
    @Column(name = "schedule_time")
    private Date scheduleTime;
    
    @JoinColumn(name = "fare")
    private Fare fare;
    
    @JoinColumn(name = "from_route")
    private Route fromRoute;
    
    @JoinColumn(name = "to_route")
    private Route toRoute;
    
    @Column(name = "recurring")
    private boolean recurring;
    
    @JoinColumn(name = "bus")
    private Bus bus;
    
}
