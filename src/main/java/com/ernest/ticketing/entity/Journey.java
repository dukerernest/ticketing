/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */

@Entity(name = "Journey")
@Getter
@Setter
public class Journey extends EntityModel{
    
    @JoinColumn(name = "ticket")
    private Ticket ticket;
    
    @Column(name = "date")
    private Date date;
}
