/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */

@Entity(name = "Fare")
@Getter
@Setter
public class Fare extends EntityModel{
    
    @Column(name = "fare_name")
    private String fareName;
    
    @Column(name = "amount")
    private double amount;
    
}
