/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import com.ernest.ticketing.entity.EntityModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */
@Entity(name = "LuggageSetting")
@Getter
@Setter
public class LuggageSetting extends EntityModel{
    
    @Column(name = "luggage_setting_name")
    private String luggageSettingName;
    
    @Column(name = "min_weight")
    private double minWeight;
    
    @Column(name = "max_weight")
    private double maxWeight;
    
    @Column(name = "unit_price")
    private double unitPrice;
}
