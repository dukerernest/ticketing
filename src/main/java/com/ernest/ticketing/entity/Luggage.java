/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */

@Entity(name = "Luggage")
@Getter
@Setter
public class Luggage extends EntityModel{
    
    @JoinColumn(name = "journey")
    private Journey journey;
    
    @JoinColumn(name = "ticket")
    private Ticket ticket;
    
    @Column(name = "tag_no")
    private String tagNo;
}
