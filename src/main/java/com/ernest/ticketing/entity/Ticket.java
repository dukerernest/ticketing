/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import com.ernest.ticketing.entity.enums.PaymentStatus;
import com.ernest.ticketing.entity.enums.TicketStatus;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */
@Entity(name = "Ticket")
@Getter
@Setter
public class Ticket extends EntityModel{
    
    @Column(name = "ticket_no")
    private String ticketNo;
    
    @Column(name = "customer_name")
    private String customerName;
    
    @JoinColumn(name = "schedule")
    private Schedule schedule;
    
    @Column(name = "ticket_date")
    private Date ticketDate;
    
    @Column(name = "payment_status")
    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;
    
    @Column(name = "seat_no")
    private int seatNo;
    
    @Column(name = "ticket_status")
    @Enumerated(EnumType.STRING)
    private TicketStatus ticketStatus;
    
    @Column(name = "amount")
    private double amount;
    
}
