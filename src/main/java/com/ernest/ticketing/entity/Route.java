/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */
@Entity(name = "Route")
@Getter
@Setter
public class Route extends EntityModel{
    
    @Column(name = "route_name")
    private String routeName;
    
    @Column(name = "route_code")
    private String routeCode;
}
