/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ernest.ticketing.controller;

import com.ernest.ticketing.TicketingService;
import com.ernest.ticketing.entity.Bus;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.UUID;
import javax.inject.Inject;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Darlington Otchere
 */
@Named(value = "busController")
@SessionScoped
public class BusController implements Serializable {

    /**
     * Creates a new instance of BusController
     */
    @Getter
    @Setter
    private Bus bus = new Bus();
    @Inject private TicketingService ticketingService;
    public BusController() {
    }
    
    public void saveBus()
    {
        if(bus.getId()==null)
        {
            bus.setId(UUID.randomUUID().toString());
            
        }
        if(ticketingService.saveMethod(bus)!=null)
        {
            //Success saved
        }
        else{
            //Error In Saving
        }
    }
    
    
}
